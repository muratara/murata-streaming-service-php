<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Storage;

class Filer
{
    /**
     * @var $directory
     */
    public static $publicDirectory = 'public/files/';

    /**
     * @param $request
     * @param $inputName
     * @return string
     */
    public static function uploadFile($request, $inputName = 'file')
    {
        $requestFile = $request->file($inputName);
        try {
            $filename = time().'.'.$requestFile->extension();
            Storage::putFileAs(self::$publicDirectory, $requestFile, $filename);
            return asset(Storage::url(self::$publicDirectory.$filename));
        } catch (\Throwable $th) {
            report($th);
            abort(500, 'Error uploading file');
        }
    }
}
