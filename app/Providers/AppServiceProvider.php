<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Interfaces\AuthInterface::class, \App\Repositories\AuthRepositorie::class
        );

        $this->app->bind(
            \App\Interfaces\TagsInterface::class, \App\Repositories\TagsRepository::class
        );

        $this->app->bind(
            \App\Interfaces\CategoryInterface::class, \App\Repositories\CategoryRepository::class
        );

        $this->app->bind(
            \App\Interfaces\FilerInterface::class, \App\Repositories\FilerRepository::class
        );

        $this->app->bind(
            \App\Interfaces\NewsInterface::class, \App\Repositories\NewsRepository::class
        );

        $this->app->bind(
            \App\Interfaces\CompanyInterface::class, \App\Repositories\CompanyRepository::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
