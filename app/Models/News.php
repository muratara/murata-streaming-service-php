<?php
namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_slug', 'slug');
    }

    public function categoryGroup()
    {
        return $this->belongsTo(GroupCategory::class, 'category_slug', 'slug');
    }

    public function tags()
    {
        return $this->hasMany(TagNews::class, 'news_id');
    }
}
