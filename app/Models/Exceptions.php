<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exceptions extends Model
{
    protected $table = 'exceptions';
    // protected $fillable = [];
    protected $guarded = [];
}
