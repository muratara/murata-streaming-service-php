<?php
namespace App\Interfaces;

use App\Http\Requests\AuthRegisterCustomerRequest;
use App\Http\Requests\AuthRequest;

interface AuthInterface
{
    public function Login(AuthRequest $request): object;

    public function Me(): object;

    public function RegisterCustomer(AuthRegisterCustomerRequest $request): object;

    public function LoginCustomer(AuthRequest $request): object;

    public function RefreshToken(): object;
}
