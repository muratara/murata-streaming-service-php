<?php
namespace App\Interfaces;

use App\Http\Requests\TagsRequest;

interface TagsInterface
{
    public function getAllTags(): object;

    public function storeTags(TagsRequest $request): object;

    public function updateTags(TagsRequest $request, int $id): object;

    public function deleteTags(int $id): object;
}
