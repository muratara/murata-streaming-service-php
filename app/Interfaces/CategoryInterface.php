<?php
namespace App\Interfaces;

use App\Http\Requests\CategoryRequest;

interface CategoryInterface
{
    public function getAllCategories(): object;

    public function storeCategories(CategoryRequest $request): object;

    public function updateCategories(CategoryRequest $request, int $id): object;

    public function deleteCategories(int $id): object;

    public function categoryOptions(): object;

    public function categoryMenu(): object;

    public function categoryDetail($slug): object;
}
