<?php

namespace App\Http\Resources;

use App\Helpers\Time;
use Illuminate\Http\Resources\Json\JsonResource;

class TopNews extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'image' => $this->image,
            'last_update' => Time::time_elapsed_string($this->created_at),
        ];
    }
}
