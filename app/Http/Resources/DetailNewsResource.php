<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DetailNewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $tags = $this->tags->map(function($item) {
                return [
                    'tag_id' => $item->tag->id,
                    'tag_name' => $item->tag->name,
                ];
            }) ?? [];
        $tagString = $tags->map(function ($tag) {
            return $tag['tag_name'];
        });
        $tagString = implode(", ", $tagString->toArray());
        return [
            'id' => $this->id,
            'title' => $this->title,
            'category_id' => $this->category_id,
            'category_name' => $this->category->name ?? $this->categoryGroup->name ?? '',
            'category_slug' => $this->category->slug ?? $this->categoryGroup->slug ?? '',
            "content" => $request->splitContent != null ? $this->splitContentSentence($this->content) . "." : $this->content,
            'image' => $this->image,
            'total_views' => $this->total_views,
            'status' => $this->status,
            'tags' => $tags,
            'tags_string' => $tagString,
            'updated_by' => $this->updatedBy->name ?? $this->user->name ?? '',
            'last_update' => date('Y-m-d H:i:s', strtotime($this->updated_at)),
        ];
    }

    public function splitContentSentence($content)
    {
        return preg_split('/[.!?]+/', $content, -1, PREG_SPLIT_NO_EMPTY)[0];
    }
}
