<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TagsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tags_name' => $this->name,
            'creatd_by' => $this->user->name ?? '',
            'updated_by' => $this->updatedBy->name ?? '',
            'last_update' => date('Y-m-d H:i:s', strtotime($this->updated_at)),
        ];
    }
}
