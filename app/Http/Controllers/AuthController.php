<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRegisterCustomerRequest;
use App\Http\Requests\AuthRequest;
use App\Interfaces\AuthInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected AuthInterface $auth;

    protected Request $request;

    public function __construct(AuthInterface $auth, Request $request)
    {
        $this->auth = $auth;
        $this->request = $request;
    }

    public function login(AuthRequest $request): JsonResponse
    {
        $resp = $this->auth->Login($request);
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function me(): JsonResponse
    {
        $resp = $this->auth->Me();
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function registerCustomer(AuthRegisterCustomerRequest $request): JsonResponse
    {
        $resp = $this->auth->RegisterCustomer($request);
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function loginCustomer(AuthRequest $request): JsonResponse
    {
        $resp = $this->auth->LoginCustomer($request);
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function refreshToken(): JsonResponse
    {
        $resp = $this->auth->RefreshToken();
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }
}
