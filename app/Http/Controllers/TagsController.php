<?php

namespace App\Http\Controllers;

use App\Http\Requests\TagsRequest;
use App\Interfaces\TagsInterface;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    protected TagsInterface $tags;

    protected Request $request;

    public function __construct(TagsInterface $tags, Request $request)
    {
        $this->tags = $tags;
        $this->request = $request;
    }

    public function getAllTags()
    {
        $resp = $this->tags->getAllTags();
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function storeTags(TagsRequest $request)
    {
        $resp = $this->tags->storeTags($request);
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function updateTags(TagsRequest $request, int $id)
    {
        $resp = $this->tags->updateTags($request, $id);
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function deleteTags(int $id)
    {
        $resp = $this->tags->deleteTags($id);
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }
}
