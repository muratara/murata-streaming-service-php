<?php

namespace App\Repositories;

use App\Helpers\Text;
use App\Http\Requests\NewsRequest;
use App\Http\Resources\AdminNewsResource;
use App\Http\Resources\DetailNewsResource;
use App\Http\Resources\LatestNewsResource;
use App\Http\Resources\TopNews;
use App\Interfaces\NewsInterface;
use App\Models\Category;
use App\Models\GroupCategory;
use App\Models\News;
use App\Models\Tag;
use App\Models\TagNews;
use Illuminate\Support\Facades\DB;

class NewsRepository extends RepositoryController implements NewsInterface
{
    public function getNews(): object
    {
        $news = News::orderBy('id', 'desc');
        if (request()->has('last_id')) {
            $news = $news->where('id', '<', request()->last_id);
        }
        $news = $news->limit($this->getLimitPage())->get();
        return $this->callback_response("success", 200, 'Get news success', [
            'data' => AdminNewsResource::collection($news),
            'next_total_data' => News::count() > $this->getLimitPage() ? News::count() - (request()->current_total ?? $this->getLimitPage()) : 0
        ]);
    }

    public function storeNews(NewsRequest $request): object
    {
        try {
            DB::beginTransaction();
            $body = $this->splitBodyNewsAndTags();
            $news = News::create($body->news);

            $splitTags = explode(',', $body->tags);
            TagNews::where('news_id', $news->id)->delete();
            foreach ($splitTags as $tag) {
                $tagNews = new TagNews();
                $tagNews->news_id = $news->id;
                $tagNews->tag_id = $tag;
                $tagNews->save();
            }

            DB::commit();
            return $this->callback_response("success", 200, 'Store news success', $news);
        } catch (\Throwable $th) {
            DB::rollBack();
            report($th);
            abort(500, $th->getMessage());
        }
    }
    public function updateNews(int $id, NewsRequest $request): object
    {
        try {
            DB::beginTransaction();
            $news = News::findOrFail($id);
            $body = $this->splitBodyNewsAndTags();
            $news->update($body->news);

            $splitTags = explode(',', $body->tags);
            TagNews::where('news_id', $news->id)->delete();
            foreach ($splitTags as $tag) {
                $tagNews = new TagNews();
                $tagNews->news_id = $news->id;
                $tagNews->tag_id = $tag;
                $tagNews->save();
            }

            DB::commit();
            return $this->callback_response("success", 200, 'Update news success', $news);
        } catch (\Throwable $th) {
            DB::rollBack();
            report($th);
            abort(500, $th->getMessage());
        }
    }
    public function deleteNews(int $id): object
    {
        try {
            $news = News::findOrFail($id);
            $news->delete();
            return $this->callback_response("success", 200, 'Delete news success', $news);
        } catch (\Throwable $th) {
            report($th);
            abort(500, $th->getMessage());
        }
    }

    public function splitBodyNewsAndTags(): object
    {
        $body = request()->all();
        $bodyNews = [];
        foreach ($body as $key => $value) {
            if ($key != 'tags') {
                $bodyNews[$key] = $value;
            }
        }

        $bodyNews['slug'] = Text::slugify($bodyNews['title']);
        $bodyNews['created_by'] = auth()->user()->id;

        $tags = $body['tags'];

        return (object)[
            'news' => $bodyNews,
            'tags' => $tags
        ];
    }

    public function readNews(int $id): object
    {
        $news = News::find($id);
        if (!$news) {
            return $this->callback_response("not_found", 404, 'News not found');
        }
        return $this->callback_response("success", 200, 'Get news success',new DetailNewsResource($news));
    }

    public function getPublishNews(): object
    {
        $news = News::where("status", "publish")->orderBy('id', 'desc');
        if (request()->has('last_id')) {
            $news = $news->where('id', '<', request()->last_id);
        }
        $news = $news->limit($this->getLimitPage())->get();
        return $this->callback_response("success", 200, 'Get news success', [
            'data' => DetailNewsResource::collection($news),
            'next_total_data' => News::count() > $this->getLimitPage() ? News::count() - (request()->current_total ?? $this->getLimitPage()) : 0
        ]);
    }

    public function latestNews(): object
    {
        $news = News::where("status", "publish")->orderBy('id', 'desc')->limit($this->getLimitPage())->get();
        return $this->callback_response("success", 200, 'Get news success', [
            'articles' => LatestNewsResource::collection($news)
        ]);
    }

    public function topNews(): object
    {
        $tags = Tag::where('name', 'Utama')->first();
        $news = News::selectRaw("news.*")
            ->leftJoin('tag_news', 'news.id', '=', 'tag_news.news_id')
            ->where('tag_news.tag_id', $tags->id)
            ->where("status", "publish");

        if (request()->slug) {
            $categories = $this->categorySlug(request()->slug);
            $news = $news->whereIn('category_slug', $categories);
        }

        $news = $news->orderBy('total_views', 'desc')->limit($this->getLimitPage())->get();
        return $this->callback_response("success", 200, 'Get news success', [
            'articles' => TopNews::collection($news)
        ]);
    }

    public function popularNews(): object
    {
        $news = News::where("status", "publish")->where('total_views', '>', 0);

        if (request()->slug) {
            $categories = $this->categorySlug(request()->slug);
            $news = $news->whereIn('category_slug', $categories);
        }

        $news = $news->orderBy('total_views', 'desc')->limit($this->getLimitPage())->get();
        return $this->callback_response("success", 200, 'Get news success', [
            'articles' => LatestNewsResource::collection($news)
        ]);
    }

    public function getNewsByCategory($categorySlug): object
    {
        $categories = $this->categorySlug($categorySlug);
        $news = News::where("status", "publish")->whereIn('category_slug', $categories)->orderBy('updated_at', 'desc')->get();
        return $this->callback_response("success", 200, 'Get news success', DetailNewsResource::collection($news));
    }

    protected function categorySlug($slug)
    {
        $categories = [];
        $category = GroupCategory::where('slug', $slug)->get();
        if (!$category) {
            $category = Category::where('slug', $slug)->get();
            foreach ($category as $cat) {
                $categories[] = $cat->slug;
            }
        } else {
            foreach ($category as $cat) {
                $categories[] = $cat->slug;
                $subCategory = Category::where('group_category', $cat->id)->get();
                foreach ($subCategory as $sub) {
                    $categories[] = $sub->slug;
                }
            }
        }
        return $categories;
    }
}
