<?php
namespace App\Repositories;

use App\Helpers\Text;
use App\Http\Requests\TagsRequest;
use App\Http\Resources\TagsResource;
use App\Interfaces\TagsInterface;
use App\Models\Tag;
use Illuminate\Support\Facades\Auth;

class TagsRepository extends RepositoryController implements TagsInterface
{

    public function getAllTags(): object
    {
        try {
            $tags = Tag::orderBy('updated_at', 'desc')
                ->when(request()->has('name'), function ($query) {
                    $query->where('name', 'like', request()->name . "%");
                });
            if (request()->has('notPaginate')) {
                $tags = $tags->get();
            } else {
                $tags = $tags->paginate($this->getLimitPage());
            }
            TagsResource::collection($tags);
            return $this->callback_response("success", 200, 'Get all tags success', $tags);
        } catch (\Exception $th) {
            report($th);
            abort(500, $th->getMessage());
        }
    }

    public function storeTags(TagsRequest $request): object
    {
        try {
            if ($this->duplicateEntry()) return $this->callback_response("duplicate_entry", 400, 'Tag is exist');

            $body = $request->all();
            $body['slug'] = Text::slugify($body['name']);
            $body['created_by'] = Auth::user()->id;
            $tag = Tag::create($body);
            return $this->callback_response("success", 200, 'Create tag success', [
                'tag' =>  new TagsResource($tag)
            ]);
        } catch (\Exception $th) {
            report($th);
            abort(500, $th->getMessage());
        }
    }

    public function updateTags(TagsRequest $request, int $id): object
    {
        try {
            $tag = Tag::findOrFail($id);

            if ($this->duplicateEntry()) return $this->callback_response("duplicate_entry", 400, 'Tag is exist');

            $body = $request->all();
            $body['slug'] = Text::slugify($body['name']);
            $body['updated_by'] = Auth::user()->id;
            $tag->update($body);
            return $this->callback_response("success", 200, 'Update tag success', [
                'tag' => new TagsResource($tag)
            ]);
        } catch (\Exception $th) {
            report($th);
            abort(500, $th->getMessage());
        }
    }

    public function deleteTags(int $id): object
    {
        try {
            $tag = Tag::findOrFail($id);
            $tag->delete();
            return $this->callback_response("success", 200, 'Delete tag success', [
                'tag' => new TagsResource($tag)
            ]);
        } catch (\Exception $th) {
            report($th);
            abort(500, $th->getMessage());
        }
    }

    protected function duplicateEntry()
    {
        $tag = Tag::where('name', request()->name)->first();
        if ($tag) return true;
        return false;
    }
}
