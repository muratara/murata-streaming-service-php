<?php
namespace App\Repositories;

use App\Http\Requests\AuthRegisterCustomerRequest;
use App\Http\Requests\AuthRequest;
use App\Interfaces\AuthInterface;
use App\Models\Customer;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthRepositorie extends RepositoryController implements AuthInterface
{
    public function Login(AuthRequest $request): object
    {
        try {
            if (!Auth::attempt(['email' => $request->username, 'password' => $request->password])) {
                return $this->callback_response("unautorized_credential", 401, 'Username or password is incorrect');
            }
            $user = Auth::user();
            return $this->callback_response("success", 200, 'Login success', [
                'token' => $user->createToken('auth_token')->plainTextToken,
                'user' => $user
            ]);
        } catch (\Exception $th) {
            report($th);
            abort(500, $th->getMessage());
        }
    }

    public function Me(): object
    {
        try {
            $user = Auth::user();
            return $this->callback_response("success", 200, 'Get user success', [
                'user' => $user
            ]);
        } catch (\Exception $th) {
            report($th);
            abort(500, $th->getMessage());
        }
    }

    public function RegisterCustomer(AuthRegisterCustomerRequest $request): object
    {
        try {
            $customerExists = User::where('email', $request->email)->where('type', 'customer')->first();
            if ($customerExists) return $this->callback_response("unautorized_credential", 200, 'Email already register', null);
            DB::beginTransaction();
            $user = new User();
            $user->name = $request->full_name;
            $user->email = $request->email;
            $user->type = 'customer';
            $user->password = bcrypt($request->password);
            $user->save();

            $customer = new Customer();
            $customer->full_name = $request->full_name;
            $customer->save();
            DB::commit();

            return $this->callback_response("success", 200, 'Register user success', $customer);
        } catch (\Exception $exception) {
            DB::rollBack();
            report($exception);
            abort(500, $exception->getMessage());
        }
    }

    public function LoginCustomer(AuthRequest $request): object
    {
        try {
            if (!Auth::attempt(['email' => $request->username, 'password' => $request->password, 'type' => 'customer'])) {
                return $this->callback_response("unautorized_credential", 401, 'Username or password is incorrect');
            }
            $user = Auth::user();
            return $this->callback_response("success", 200, 'Login success', [
                'token' => $user->createToken('auth_token')->plainTextToken,
                'user' => $user
            ]);
        } catch (\Exception $th) {
            report($th);
            abort(500, $th->getMessage());
        }
    }

    public function RefreshToken(): object
    {
        $user = request()->user();
        $user->tokens()->delete();
        return $this->callback_response("success", 200, 'Login success', [
            'token' => $user->createToken('auth_token')->plainTextToken,
            'user' => $user
        ]);
    }
}

