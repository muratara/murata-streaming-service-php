<?php
namespace App\Repositories;

class RepositoryController
{
    public $defaultLimit = 10;

    public function callback_response($status = "success", $code = 200, $message = '', $data = null)
    {
        return (object)[
            'status' => $status,
            'code' => $code,
            'message' => $message,
            'data' => $data
        ];
    }

    public function getLimitPage()
    {
        return request()->has('limit') ? request()->get('limit') : $this->defaultLimit;
    }
}
