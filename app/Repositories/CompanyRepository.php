<?php

namespace App\Repositories;

use App\Interfaces\CompanyInterface;
use App\Models\Company;
use App\Models\SocialMedia;

class CompanyRepository extends RepositoryController implements CompanyInterface
{
    public function company(): object
    {
        $company = Company::first();
        $company->social_medias = SocialMedia::all();
        return $this->callback_response("success", 200, 'Get company success', $company);
    }
}
